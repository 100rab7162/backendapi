import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class MainApp {
    public static void main(String[] args) {

    }
    public synchronized void  assignOrderToChefQueue(Order order){
        Integer queueSize = null;
        Queue<Order> queue = null;
        Integer min = 100000;
        Chef chefKey = null;
        Set<Map.Entry<Chef,Queue<Order>>> chefSet = chefMap.entrySet();
        for(Map.Entry<Chef,Queue<Order>> entry : chefSet){
            queue = entry.getValue();
            queueSize = queue.size();
            if(queueSize == 0){
                queue.offer(order);
                break;
            }
            else if( queueSize == chefQueueSize){
                continue;
            }
            else if(queueSize < min){
                min = queueSize;
                chefKey = entry.getKey();
            }
        }

        if(null != chefKey){
            Queue queue1 = chefMap.get(chefKey);
            queue1.offer(order);
        }
    }
}
