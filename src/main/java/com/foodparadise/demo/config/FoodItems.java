package com.foodparadise.demo.config;

import com.foodparadise.demo.app.model.FoodCalorieItem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FoodItems {
    private List<FoodCalorieItem> items = new ArrayList<>();

    public List<FoodCalorieItem> getItems() {
        return items;
    }

    public void setItems(List<FoodCalorieItem> items) {
        this.items = items;
    }
}
