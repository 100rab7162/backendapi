package com.foodparadise.demo.config;

import com.foodparadise.demo.app.model.FoodCalorieItem;
import com.foodparadise.demo.app.model.UserCredential;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class UserSession {
    @Bean
    @Scope("singleton")
    public Map<String, UserCredential> getUserSessionMap(){
//        Map<String,UserCredential> userSessioMap = new HashMap<>();
        return new HashMap<>();
    }
}
