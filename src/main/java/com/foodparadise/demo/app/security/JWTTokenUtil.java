package com.foodparadise.demo.app.security;
import com.foodparadise.library.JWTToken;
import com.foodparadise.demo.app.model.UserCredential;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JWTTokenUtil implements Serializable {

    @Value("${jwt.token.validity}")
    private Integer tokenValidity;

    @Value("${jwt.secret}")
    private String secret;

    JWTToken jwtToken;

    @PostConstruct
    public void init(){
        this.jwtToken = new JWTToken();
    }


    //retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        return jwtToken.getClaimFromToken(token, Claims::getSubject);
    }




    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = jwtToken.getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //TODO
    //generate token for user
    public String generateToken(UserCredential userCredential) {
        Map<String, Object> claims = new HashMap<>();
        return jwtToken.doGenerateToken(claims,userCredential.getUsername(),tokenValidity);
    }

    //TODO
    //1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    //2. Sign the JWT using the HS512 algorithm and secret key.
    //3. Compaction of the JWT to a URL-safe string
//    private String doGenerateToken(Map<String, Object> claims, String subject) {
//        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
//                .setExpiration(new Date(System.currentTimeMillis() + tokenValidity * 1000))
//                .signWith(SignatureAlgorithm.HS512, secret).compact();
//    }

    /**
     * @param token - present in header of the client for validation
     * @return - true if the token is valid and false if it has expired
     */
    //validate token
    public Boolean validateToken(String token) {
        return (!isTokenExpired(token));
    }
    //validate token along with userCredential
    public Boolean validateToken(String token,UserCredential userCredential) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userCredential.getUsername()) && !isTokenExpired(token));
    }





}
