package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.FoodCalorieItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class RecommendItemMapper implements RowMapper<FoodCalorieItem> {
    @Override
    public FoodCalorieItem mapRow(ResultSet rs, int i) throws SQLException {
        FoodCalorieItem foodCalorieItem = new FoodCalorieItem();
        foodCalorieItem.setProductName(rs.getString("product_name"));
        foodCalorieItem.setIngridients(rs.getString("ingridents_text"));
        foodCalorieItem.setServingSize(rs.getString("serving_size"));
        foodCalorieItem.setEnergy_100g(rs.getDouble("energy_100g"));
        foodCalorieItem.setFat_100g(rs.getDouble("fat_100g"));

        foodCalorieItem.setSaturated_fat_100g(rs.getDouble("saturated_fat_100g"));
        foodCalorieItem.setCholesterol_100g(rs.getDouble("cholesterol_100g"));
        foodCalorieItem.setCarbohydrates_100g(rs.getDouble("carbohydrates_100g"));
        foodCalorieItem.setSugar_100g(rs.getDouble("sugars_100g"));
        foodCalorieItem.setFiber_100g(rs.getDouble("fiber_100g"));
        foodCalorieItem.setProteins_100g(rs.getDouble("proteins_100g"));
        foodCalorieItem.setSalt_100g(rs.getDouble("salt_100g"));
        foodCalorieItem.setSodium_100g(rs.getDouble("sodium_100g"));
        foodCalorieItem.setCalcium_100g(rs.getDouble("calcium_100g"));
        foodCalorieItem.setIron_100g(rs.getDouble("iron_100g"));
        return foodCalorieItem;
    }
}
