package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.Chef;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChefRepository extends JpaRepository<Chef,Integer> {

}
