package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item,Integer> {

    Item findItemById(Integer id);

}
