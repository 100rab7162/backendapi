package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.FoodCalorieItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class ItemCalorieImplRepository implements ItemCalorieRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<FoodCalorieItem> getAllFoods() {
        String sql = "select product_name, ingridents_text, serving_size, energy_100g, fat_100g,saturated_fat_100g, cholesterol_100g, carbohydrates_100g, sugars_100g, fiber_100g, proteins_100g, salt_100g, sodium_100g, calcium_100g, iron_100g from recommend_items;";


        List<FoodCalorieItem> foodItems = jdbcTemplate.query(sql,new RecommendItemMapper());

        return foodItems;
    }
}
