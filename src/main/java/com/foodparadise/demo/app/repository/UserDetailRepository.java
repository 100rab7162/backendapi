package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.UserCredential;
import com.foodparadise.demo.app.model.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDetailRepository extends JpaRepository<UserDetail,Integer> {

    UserDetail getUserDetailByEmail(String email);

}
