package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.OrderItemMapping;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class Order_Item_mapper implements RowMapper<OrderItemMapping> {

    @Override
    public OrderItemMapping mapRow(ResultSet rs, int i) throws SQLException {
        OrderItemMapping orderItemMapping = new OrderItemMapping();
        orderItemMapping.setOrderId(rs.getInt("order_id"));
        orderItemMapping.setItemId(rs.getInt("item_id"));
        orderItemMapping.setRating(rs.getInt("rating"));
        return orderItemMapping;
    }
}
