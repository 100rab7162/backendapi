package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCredentialRepository extends JpaRepository<UserCredential,Integer> {
    UserCredential findByUsernameAndPassword(String username,String password);
    UserCredential findByUsername(String username);
}
