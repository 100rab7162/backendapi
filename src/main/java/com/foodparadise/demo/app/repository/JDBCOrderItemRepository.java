package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.dto.UpdateOrderDto;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Repository
public class JDBCOrderItemRepository implements OrderItemRepository {

    private static final Logger logger = LoggerFactory.getLogger(JDBCOrderItemRepository.class);

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Override
    public int save(OrderItemMapping orderItemMapping) {
        String ps = "insert into order_item(order_id,item_id,count) values(?,?,?)";
        try{
            return jdbcTemplate.update(ps,orderItemMapping.getOrderId(),orderItemMapping.getItemId(),orderItemMapping.getCount());
        }catch (DataAccessException e){
            logger.error("Error while inserting record to database :: " + e.getMessage());
        }

        return -1;
    }

    @Override
    public List<OrderItemMapping> findItemsByOrderId(List<Order> orders) {
        String sql = "select order_id,item_id,rating from order_item where order_id=?";
        List<OrderItemMapping> mainOrderItemMappings = new ArrayList<>();
        for(Order order:orders){
            List<OrderItemMapping> orderItemMappings = jdbcTemplate.query("select order_id,item_id,rating from order_item where order_id=?",new Object[]{order.getId()},new Order_Item_mapper());
            mainOrderItemMappings.addAll(orderItemMappings);
        }
        return mainOrderItemMappings;
    }

    @Override
    public List<OrderItemMapping> findAll() {
        String sql = "select order_id,item_id,rating from order_item";
        List<OrderItemMapping> mainOrderItemMappings = new ArrayList<>();

        List<OrderItemMapping> orderItemMappings = jdbcTemplate.query(sql,new Order_Item_mapper());
        mainOrderItemMappings.addAll(orderItemMappings);

        return mainOrderItemMappings;
    }

    @Override
    public void updateItemRating(UpdateOrderDto updateOrderDto) {
        String sql = "UPDATE foodzilla.order_item SET rating = ? WHERE order_id=? and item_id=?;";
        try{
            jdbcTemplate.update(sql,updateOrderDto.getNewRating(),updateOrderDto.getOrderId(),updateOrderDto.getItemId());
        }catch (DataAccessException e){
            logger.error("Error while inserting record to database :: " + e.getMessage());
        }
    }

    @Override
    public List<Item> findItemsByOrderId(int id) {
        return null;
    }

    @Override
    public int[] batchInsert(List<OrderItemMapping> orderItemMappings) {
        return jdbcTemplate.batchUpdate(
                "insert into order_item(order_id,item_id,count) values(?,?,?)",
                new BatchPreparedStatementSetter() {

                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, orderItemMappings.get(i).getOrderId());
                        ps.setInt(2, orderItemMappings.get(i).getItemId());
                        ps.setInt(3, orderItemMappings.get(i).getCount());
                    }

                    public int getBatchSize() {
                        return orderItemMappings.size();
                    }

                });

    }
}
