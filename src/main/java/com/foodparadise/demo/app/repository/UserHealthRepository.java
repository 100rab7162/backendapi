package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.UserHealth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHealthRepository extends JpaRepository<UserHealth,Integer> {
    
}
