package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.dto.UpdateOrderDto;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import org.springframework.stereotype.Repository;


import java.util.List;
//@Repository
public interface OrderItemRepository {

    int save(OrderItemMapping orderItemMapping);

    List<OrderItemMapping> findAll();

    List<Item> findItemsByOrderId(int id);

    int[] batchInsert(List<OrderItemMapping> orderItemMappings);

    List<OrderItemMapping> findItemsByOrderId(List<Order> orders);

    void updateItemRating(UpdateOrderDto updateOrderDto);

}
