package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Integer> {
}
