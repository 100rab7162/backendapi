package com.foodparadise.demo.app.repository;

import com.foodparadise.demo.app.model.FoodCalorieItem;

import java.util.List;

public interface ItemCalorieRepository {
    public List<FoodCalorieItem> getAllFoods();
}
