package com.foodparadise.demo.app.controller;

import com.foodparadise.demo.app.dto.*;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.recommender.RecommendByCategory;
import com.foodparadise.demo.app.recommender.RecommenderSystem;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.app.service.ItemService;
import com.foodparadise.demo.app.service.OrderService;
import com.foodparadise.demo.app.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    ItemService itemService;

    @Autowired
    OrderService orderService;

    @Autowired
    JWTTokenUtil jwtTokenUtil;

    @Autowired
    RecommenderSystem recommenderSystem;

    @Autowired
    RecommendByCategory recommendByCategory;

    /**
     * @param registrationDto
     * @return
     */
    @PostMapping(value="/user/register")
    public @ResponseBody ResponseEntity registerUser(@RequestBody @Valid RegistrationDto registrationDto, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            List<String> errorList = getErrorList(bindingResult);
            return new ResponseEntity(errorList,HttpStatus.PARTIAL_CONTENT);
        }
        else{
            userService.registerUser(registrationDto);
            logger.info(registrationDto.getFirstName());
            return new ResponseEntity(registrationDto,HttpStatus.OK);
        }
    }

    @PostMapping(value="/user/health")
    public @ResponseBody ResponseEntity healthCheckUser(@RequestBody @Valid UserHealthDto userHealthDto, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            List<String> errorList = getErrorList(bindingResult);
            return new ResponseEntity(errorList,HttpStatus.PARTIAL_CONTENT);
        }
        else{
            UserHealthResponseDto userHealthResponseDto = userService.getUserRecommendation(userHealthDto);
            return new ResponseEntity(userHealthResponseDto,HttpStatus.OK);
        }
    }


    @PostMapping(value="/user/login")
    public @ResponseBody ResponseEntity loginUser(@RequestBody @Valid UserLoginRequestDto userLoginRequestDto, BindingResult bindingResult){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Access-Control-Allow-Origin","*");
        if(bindingResult.hasErrors()){
            List<String> errorList = getErrorList(bindingResult);
            return new ResponseEntity(errorList,HttpStatus.PARTIAL_CONTENT);
        }
        else{
            UserResponseDto userResponseDto = userService.authenticateUser(userLoginRequestDto);
            logger.info("This works!!!");
             return new ResponseEntity(userResponseDto,HttpStatus.OK);
        }
    }

    @PostMapping(value="/user/authenticate/placeorder")
    public @ResponseBody ResponseEntity placeOrder(@RequestBody List<OrderRequestDto> orderList,HttpServletRequest httpServletRequest){
        Integer generatedOrderID = null;
        String username = getUserNameFromToken(httpServletRequest);
        generatedOrderID = orderService.registerOrder(orderList,username);

        if(null == generatedOrderID){
            return new ResponseEntity(new OrderResponseDto(generatedOrderID,false), HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity(new OrderResponseDto(generatedOrderID,true), HttpStatus.OK);
        }
     }

    @PostMapping(value="/user/authenticate/updateorder")
    public @ResponseBody ResponseEntity updateOrderItemRating(@RequestBody UpdateOrderDto updateOrderDto,HttpServletRequest httpServletRequest){
        orderService.updateOrderItemRating(updateOrderDto);
        return new ResponseEntity(null,null, HttpStatus.OK);
    }

    @GetMapping(value="/user/recommendbyrating")
    public @ResponseBody ResponseEntity getRecommendations(){
        List<Item> items = recommenderSystem.recommendItems();
        return new ResponseEntity( items,HttpStatus.OK);
    }

    @PostMapping(value="/user/recommendbycategory")
    public @ResponseBody ResponseEntity getRecommendationsByCategory(@RequestBody RecommendationByCategoryDto recommendationByCategoryDto, HttpServletRequest httpServletRequest){
        String username = getUserNameFromToken(recommendationByCategoryDto.getToken());
        List<Item> items = userService.getRecommendationByCategory(username,recommendationByCategoryDto.getSortby());
        return new ResponseEntity( items,HttpStatus.OK);
    }


    @GetMapping(value="/user/authenticate/productlist")
    public @ResponseBody ResponseEntity getProducts(){
        List<Item> items = itemService.getItems();
        return new ResponseEntity(items, HttpStatus.OK);
    }

    @GetMapping(value="/user/authenticate/orderlist")
    public @ResponseBody ResponseEntity getOrders(HttpServletRequest httpServletRequest){
        String username = getUserNameFromToken(httpServletRequest);
        List<OrderItemMapping> items = orderService.getOrders(username);
        return new ResponseEntity(items, HttpStatus.OK);
    }

    @GetMapping(value="/user/authenticate/logout")
    public @ResponseBody ResponseEntity logout(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("auth_token");
        String username = jwtTokenUtil.getUsernameFromToken(token);
        userService.logoutUser(username);
        logger.info("authentication is working!!");
        ResponseMessageDto responseMessageDto = new ResponseMessageDto("bakwass");

        return new ResponseEntity(responseMessageDto, HttpStatus.OK);
    }

    public List<String> getErrorList(BindingResult bindingResult) {
        List<FieldError> errors = bindingResult.getFieldErrors();
        List<String> errorList = new ArrayList<>(10);
        for (FieldError e : errors) {
            errorList.add("ERROR : " + e.getField() + " has error with message " + e.getDefaultMessage());
        }
        return errorList;
    }

    public String getUserNameFromToken(HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("auth_token");
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return username;
    }
    public String getUserNameFromToken(String token){
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return username;
    }

}
