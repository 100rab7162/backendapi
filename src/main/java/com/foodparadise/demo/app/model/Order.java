package com.foodparadise.demo.app.model;

import com.foodparadise.demo.app.model.enumeration.OrderStatus;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "orders")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    private OrderStatus orderStatus;

    @Column(name = "creation_time")
    private Timestamp creationTimestamp;

    @Column(name = "deletion_timestamp")
    private Timestamp deletionTimestamp;

    public Order() {
        this.creationTimestamp = new Timestamp(System.currentTimeMillis());
        this.orderStatus = OrderStatus.PENDING;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Timestamp getDeletionTimestamp() {
        return deletionTimestamp;
    }

    public void setDeletionTimestamp(Timestamp deletionTimestamp) {
        this.deletionTimestamp = deletionTimestamp;
    }
}
