package com.foodparadise.demo.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

//@Entity
//@Table(name = "order_item")
public class OrderItemMapping {
//    @Column(name = "order_id")
    private Integer orderId;

//    @Column(name = "item_id")
    private Integer itemId;

//    @Column(name = "count")
    private Integer count;

    private Integer rating;


    public OrderItemMapping(){

    }


    public OrderItemMapping(Integer orderId, Integer itemId, Integer count,Integer rating) {
        this.orderId = orderId;
        this.itemId = itemId;
        this.count = count;
        this.rating = rating;
    }
    public OrderItemMapping(Integer orderId, Integer itemId, Integer count) {
        this.orderId = orderId;
        this.itemId = itemId;
        this.count = count;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
