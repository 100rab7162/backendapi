package com.foodparadise.demo.app.model;

import java.io.Serializable;

//@Entity
//@Table(name = "food_calorie_item")
public class FoodCalorieItem implements Serializable {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @Column(name = "product_name")
    private String productName;

//    @Column(name = "quantity")
    private String quantity;

//    @Column(name = "ingridients_text")
    private String ingridients;

//    @Column(name = "serving_size")
    private String servingSize;

//    @Column(name = "energy_100g")
    private double energy_100g;

//    @Column(name = "fat_100g")
    private double fat_100g;

//    @Column(name = "saturated-fat_100g")
    private double saturated_fat_100g;

//    @Column(name = "cholesterol_100g")
    private double cholesterol_100g;

//    @Column(name = "carbohydrates_100g")
    private double carbohydrates_100g;

//    @Column(name = "sugar_100g")
    private double sugar_100g;

//    @Column(name = "fiber_100g")
    private double fiber_100g;

//    @Column(name = "proteins_100g")
    private double proteins_100g;

//    @Column(name = "salt_100g")
    private double salt_100g;

//    @Column(name = "sodium_100g")
    private double sodium_100g;

//    @Column(name = "calcium_100g")
    private double calcium_100g;

//    @Column(name = "iron_100g")
    private double iron_100g;



    public FoodCalorieItem(){

    }

    public FoodCalorieItem(String productName, String quantity, String ingridients, String servingSize, double energy_100g, double fat_100g, double saturated_fat_100g, double cholesterol_100g, double carbohydrates_100g, double sugar_100g, double fiber_100g, double proteins_100g, double salt_100g, double sodium_100g, double calcium_100g, double iron_100g) {
        this.productName = productName;
        this.quantity = quantity;
        this.ingridients = ingridients;
        this.servingSize = servingSize;
        this.energy_100g = energy_100g;
        this.fat_100g = fat_100g;
        this.saturated_fat_100g = saturated_fat_100g;
        this.cholesterol_100g = cholesterol_100g;
        this.carbohydrates_100g = carbohydrates_100g;
        this.sugar_100g = sugar_100g;
        this.fiber_100g = fiber_100g;
        this.proteins_100g = proteins_100g;
        this.salt_100g = salt_100g;
        this.sodium_100g = sodium_100g;
        this.calcium_100g = calcium_100g;
        this.iron_100g = iron_100g;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getIngridients() {
        return ingridients;
    }

    public void setIngridients(String ingridients) {
        this.ingridients = ingridients;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public double getEnergy_100g() {
        return energy_100g;
    }

    public void setEnergy_100g(double energy_100g) {
        this.energy_100g = energy_100g;
    }

    public double getFat_100g() {
        return fat_100g;
    }

    public void setFat_100g(double fat_100g) {
        this.fat_100g = fat_100g;
    }

    public double getSaturated_fat_100g() {
        return saturated_fat_100g;
    }

    public void setSaturated_fat_100g(double saturated_fat_100g) {
        this.saturated_fat_100g = saturated_fat_100g;
    }

    public double getCholesterol_100g() {
        return cholesterol_100g;
    }

    public void setCholesterol_100g(double cholesterol_100g) {
        this.cholesterol_100g = cholesterol_100g;
    }

    public double getCarbohydrates_100g() {
        return carbohydrates_100g;
    }

    public void setCarbohydrates_100g(double carbohydrates_100g) {
        this.carbohydrates_100g = carbohydrates_100g;
    }

    public double getSugar_100g() {
        return sugar_100g;
    }

    public void setSugar_100g(double sugar_100g) {
        this.sugar_100g = sugar_100g;
    }

    public double getFiber_100g() {
        return fiber_100g;
    }

    public void setFiber_100g(double fiber_100g) {
        this.fiber_100g = fiber_100g;
    }

    public double getProteins_100g() {
        return proteins_100g;
    }

    public void setProteins_100g(double proteins_100g) {
        this.proteins_100g = proteins_100g;
    }

    public double getSalt_100g() {
        return salt_100g;
    }

    public void setSalt_100g(double salt_100g) {
        this.salt_100g = salt_100g;
    }

    public double getSodium_100g() {
        return sodium_100g;
    }

    public void setSodium_100g(double sodium_100g) {
        this.sodium_100g = sodium_100g;
    }

    public double getCalcium_100g() {
        return calcium_100g;
    }

    public void setCalcium_100g(double calcium_100g) {
        this.calcium_100g = calcium_100g;
    }

    public double getIron_100g() {
        return iron_100g;
    }

    public void setIron_100g(double iron_100g) {
        this.iron_100g = iron_100g;
    }
}
