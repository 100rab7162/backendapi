package com.foodparadise.demo.app.model;

import com.foodparadise.demo.app.dto.enumeration.UserType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "user_credential")
public class UserCredential implements Serializable {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "username", nullable = false)
    private String username;
    @NotNull
    @Column(name = "password", nullable = false)
    private String password;
    @NotNull
    @Column(name = "type", nullable = false)
    private UserType type;

    @NotNull
    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "creation_timestamp")
    private Timestamp creationTimestamp;

    @Column(name = "deletion_timestamp")
    private Timestamp deletionTimestamp;


    public UserCredential() {
    }

    public UserCredential(@NotNull String username, @NotNull String password, @NotNull UserType type) {
        this.username = username;
        this.password = password;
        this.type = type;
        this.isActive = true;
        this.creationTimestamp = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Timestamp getDeletionTimestamp() {
        return deletionTimestamp;
    }

    public void setDeletionTimestamp(Timestamp deletionTimestamp) {
        this.deletionTimestamp = deletionTimestamp;
    }
}
