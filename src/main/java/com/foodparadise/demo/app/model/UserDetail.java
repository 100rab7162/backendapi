package com.foodparadise.demo.app.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "user_detail")
public class UserDetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;


    @Column(name = "address")
    private String address;
    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "dob")
    private Timestamp dob;
    @NotNull
    @Column(name = "email")
    private String email;
    @NotNull
    @Column(name = "mobile")
    private String mobile;


    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private UserCredential userCredential;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private UserHealth userHealth;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Order> orders;

    public UserDetail(){

    }

    public UserDetail(@NotNull String firstName, @NotNull String lastName, String address, String ipAddress, Timestamp dob, @NotNull String email, @NotNull String mobile, UserCredential userCredential) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ipAddress = ipAddress;
        this.dob = dob;
        this.email = email;
        this.mobile = mobile;
        this.userCredential = userCredential;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public UserCredential getUserCredential() {
        return userCredential;
    }

    public void setUserCredential(UserCredential userCredential) {
        this.userCredential = userCredential;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public UserHealth getUserHealth() {
        return userHealth;
    }

    public void setUserHealth(UserHealth userHealth) {
        this.userHealth = userHealth;
    }
}
