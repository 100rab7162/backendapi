package com.foodparadise.demo.app.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "shop")
public class Shop implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "name")
    private String Name;


    @NotNull
    @Column(name = "address")
    private String address;

    @NotNull
    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "location")
    private String location;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Item> items;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Item> orders;

    public Shop(){

    }

    public Shop(@NotNull String name, @NotNull String address, @NotNull String ipAddress, String location, List<Item> items) {
        Name = name;
        this.address = address;
        this.ipAddress = ipAddress;
        this.location = location;
        this.items = items;
    }
}
