package com.foodparadise.demo.app.validator;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DOBValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DOBConstraint {
    String message() default "Invalid DOB format";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
