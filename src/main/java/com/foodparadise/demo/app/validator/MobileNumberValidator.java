package com.foodparadise.demo.app.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class  MobileNumberValidator implements ConstraintValidator<MobileNumberConstraint,String> {
    @Override
    public boolean isValid(String mobileNumber, ConstraintValidatorContext constraintValidatorContext) {
        return mobileNumber != null && mobileNumber.matches("[0-9]+")
                && (mobileNumber.length() > 8) && (mobileNumber.length() < 14);

    }

    public void initialize(MobileNumberValidator constraintAnnotation) {

    }
}
