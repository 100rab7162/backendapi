package com.foodparadise.demo.app.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class DOBValidator implements ConstraintValidator<DOBConstraint,String> {
    @Override
    public boolean isValid(String dob, ConstraintValidatorContext constraintValidatorContext) {
        Pattern DATE_PATTERN = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
        return DATE_PATTERN.matcher(dob).matches();
    }
}
