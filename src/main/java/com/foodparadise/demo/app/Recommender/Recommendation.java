package com.foodparadise.demo.app.recommender;

import com.foodparadise.demo.app.model.Item;

import java.util.List;

public interface Recommendation {
    public List<Item> recommendItems(String id, String sortBy);

}
