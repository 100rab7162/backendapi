package com.foodparadise.demo.app.recommender;

import com.foodparadise.demo.app.model.FoodCalorieItem;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.repository.ItemCalorieImplRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RecommendByCalorieIntake {

    @Autowired
    ItemCalorieImplRepository itemCalorieImplRepository;

    private List<FoodCalorieItem> items;


    @PostConstruct
    public void init(){
        this.items = new ArrayList<>();
    }

    public List<FoodCalorieItem> recommendItems(String lifeStyle) {
        if(items.isEmpty()){
            items.addAll(itemCalorieImplRepository.getAllFoods());
        }

        List<FoodCalorieItem> items = null;

        switch (lifeStyle){
            case "sedentary": items = recommendationsForSedentaryLifeStyle();
                break;
            case "slightly_active_lifestyle": items = recommendationForSlightlyActiveLifeStyle();
                break;
            case "moderately_active_lifestyle": items = recommendationsForModeratelyActiveLifeStyle();
                break;
            case "active_lifestyle" : items = recommendationsForActiveLifeStyles();
                break;
            case "very_active_lifestyle":items = recommendationsForVeryActiveLifeStyle();
                break;

        }

        return items;
    }

    public List<FoodCalorieItem> recommendationsForSedentaryLifeStyle(){
        List<FoodCalorieItem> proteinList = items.stream().filter((FoodCalorieItem i) -> i.getProteins_100g()>20 && i.getProteins_100g()<40).collect(Collectors.toList());
        List<FoodCalorieItem> carbohydratesList = proteinList.stream().filter((FoodCalorieItem i) -> i.getCarbohydrates_100g()>20 && i.getCarbohydrates_100g()<50).collect(Collectors.toList());
        List<FoodCalorieItem> finalList = items.stream().filter((FoodCalorieItem i)-> i.getCalcium_100g()>10 && i.getCalcium_100g()<30).collect(Collectors.toList());
        finalList.addAll(carbohydratesList);
        return finalList;


    }
    public List<FoodCalorieItem> recommendationForSlightlyActiveLifeStyle(){
        List<FoodCalorieItem> proteinList = items.stream().filter((FoodCalorieItem i) -> i.getProteins_100g()>30 && i.getProteins_100g()<50).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> carbohydratesList = items.stream().filter((FoodCalorieItem i) -> i.getCarbohydrates_100g()>60).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> finalList = items.stream().filter((FoodCalorieItem i)-> i.getCalcium_100g()>10 && i.getCalcium_100g()<30).limit(10).collect(Collectors.toList());
        finalList.addAll(carbohydratesList);
        finalList.addAll(proteinList);

        return finalList;
    }
    public List<FoodCalorieItem> recommendationsForModeratelyActiveLifeStyle(){
        List<FoodCalorieItem> proteinList = items.stream().filter((FoodCalorieItem i) -> i.getProteins_100g()>50 && i.getProteins_100g()<80).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> carbohydratesList = items.stream().filter((FoodCalorieItem i) -> i.getCarbohydrates_100g()>50 && i.getCarbohydrates_100g()<60).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> finalList = items.stream().filter((FoodCalorieItem i)-> i.getCalcium_100g()>30).limit(10).collect(Collectors.toList());
        finalList.addAll(carbohydratesList);
        finalList.addAll(proteinList);
        return finalList;
    }
    public List<FoodCalorieItem> recommendationsForActiveLifeStyles(){
        List<FoodCalorieItem> proteinList = items.stream().filter((FoodCalorieItem i) -> i.getProteins_100g()>60).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> carbohydratesList = items.stream().filter((FoodCalorieItem i) -> i.getCarbohydrates_100g()>60).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> finalList = items.stream().filter((FoodCalorieItem i)-> i.getCalcium_100g()>30).limit(10).collect(Collectors.toList());
        finalList.addAll(carbohydratesList);
        finalList.addAll(proteinList);
        return finalList;
    }
    public List<FoodCalorieItem> recommendationsForVeryActiveLifeStyle(){

        List<FoodCalorieItem> proteinList = items.stream().filter((FoodCalorieItem i) -> i.getProteins_100g()>80).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> carbohydratesList = items.stream().filter((FoodCalorieItem i) -> i.getCarbohydrates_100g()>70 && i.getCarbohydrates_100g()<90).limit(10).collect(Collectors.toList());
        List<FoodCalorieItem> finalList = items.stream().filter((FoodCalorieItem i)-> i.getCalcium_100g()>40 && i.getCalcium_100g()<60).limit(10).collect(Collectors.toList());
        finalList.addAll(carbohydratesList);
        finalList.addAll(proteinList);


        return finalList;


    }
}
