package com.foodparadise.demo.app.recommender;

import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.model.UserDetail;
import com.foodparadise.demo.app.repository.ItemRepository;
import com.foodparadise.demo.app.repository.JDBCOrderItemRepository;
import com.foodparadise.demo.app.repository.UserDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RecommendByCategory implements Recommendation {

    @Autowired
    UserDetailRepository userDetailRepository;

    @Autowired
    JDBCOrderItemRepository jdbcOrderItemRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public List<Item> recommendItems(String id,String sortBy) {
        UserDetail userDetail = userDetailRepository.getUserDetailByEmail(id);
        List<Order> orders = userDetail.getOrders();

        Set<Item> items = new HashSet<>();

        List<OrderItemMapping> orderItems =   jdbcOrderItemRepository.findItemsByOrderId(orders);
        orderItems.sort((OrderItemMapping o1, OrderItemMapping o2)-> o2.getRating().compareTo(o1.getRating()));


        orderItems.forEach((OrderItemMapping orderItem)->{
            Item item = itemRepository.findItemById(orderItem.getItemId());
            if(items.size() <= 5){
                items.add(item);
            }
        });

        List<Item> allItems = itemRepository.findAll();
        Set<Item> recommendedItemList = new HashSet<>();

        List<OrderItemMapping> allOrderItems = jdbcOrderItemRepository.findAll();

        allOrderItems.forEach((OrderItemMapping x) -> {
            if(x.getRating() > 0){
                for(Item item:allItems){
                    if(item.getSpicy().equals("true") && sortBy.equals("spicy")){
                        recommendedItemList.add(item);
                    }
                    else if(item.getGluten().equals("true") && sortBy.equals("gluten")){
                        recommendedItemList.add(item);
                    }
                    else if(item.getVeg().equals("true") && sortBy.equals("veg")){
                        recommendedItemList.add(item);
                    }
                }
            }
        });

        //Remove already existing values from Items

        for(Item item:items){
            Iterator<Item> iterator = recommendedItemList.iterator();

            while(iterator.hasNext()){
                Item x = iterator.next();
                if(item.getId()==x.getId()){
                    iterator.remove();
                }
            }
        }

        return new ArrayList<>(recommendedItemList);
    }
}
