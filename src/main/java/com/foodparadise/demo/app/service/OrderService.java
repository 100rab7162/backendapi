package com.foodparadise.demo.app.service;

import com.foodparadise.demo.app.dto.OrderRequestDto;
import com.foodparadise.demo.app.dto.UpdateOrderDto;
import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.model.UserCredential;
import com.foodparadise.demo.app.model.UserDetail;
import com.foodparadise.demo.app.repository.*;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.app.service.order.AddOrderToDatabase;
import com.foodparadise.demo.app.service.order.AddOrderToQueue;
import com.foodparadise.demo.app.service.order.OrderListener;
import org.hibernate.sql.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    private List<OrderListener> orderListeners;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserDetailRepository userDetailRepository;

    @Autowired
    UserCredentialRepository userCredentialRepository;

    @Autowired
    JWTTokenUtil jwtTokenUtil;

    @Autowired
    JDBCOrderItemRepository jdbcOrderItemRepository;

    private AddOrderToDatabase addOrderToDatabase;
    private AddOrderToQueue addOrderToQueue;

    @Autowired
    public void setAddOrderToDatabase(AddOrderToDatabase addOrderToDatabase) {
        this.addOrderToDatabase = addOrderToDatabase;
    }

    @Autowired
    public void setAddOrderToQueue(AddOrderToQueue addOrderToQueue) {
        this.addOrderToQueue = addOrderToQueue;
    }

    @PostConstruct
    public void init(){
        orderListeners = new ArrayList<>();
//        orderListeners.add(addOrderToDatabase);
        orderListeners.add(addOrderToQueue);
    }


    public Integer registerOrder(List<OrderRequestDto> orderList,String username){

        UserCredential userCredential =  null;
        UserDetail userDetail = null;
        List<Order> orders = null;
        Integer orderId = null;


        try {
            Order order = orderRepository.save(new Order());
//            userCredential = userCredentialRepository.findByUsername(username);
            userDetail = userDetailRepository.getUserDetailByEmail(username);
            orders = userDetail.getOrders();
            orders.add(order);
            userDetailRepository.saveAndFlush(userDetail);

            List<OrderItemMapping> orderItemMappings = new ArrayList<>();

            orderList.forEach(orderItem -> orderItemMappings.add(new OrderItemMapping(order.getId(),orderItem.getItemId(),orderItem.getCount())));

            for(OrderListener orderListener : orderListeners){
                orderListener.placeOrder(order,orderItemMappings);
            }


            orderId = order.getId();
        } catch (Exception e) {
            logger.error("Failed to make the save the order to the database :: " + e.getMessage());
        }
        return orderId;
    }

    public void updateOrderItemRating(UpdateOrderDto updateOrderDto){
        jdbcOrderItemRepository.updateItemRating(updateOrderDto);
    }

    public List<OrderItemMapping> getOrders(String username){
        UserDetail userDetail = null;
        List<Order> orders = null;
        List<OrderItemMapping> orderItemMappings = null;
        try{
            userDetail = userDetailRepository.getUserDetailByEmail(username);
            orders = userDetail.getOrders();
            orderItemMappings = jdbcOrderItemRepository.findItemsByOrderId(orders);



        }catch (Exception e){
            logger.error("Error while fetching order and item mapping for user ::" + username + " :: with message:: " +e.getMessage() );
        }

        return orderItemMappings;
    }


}
