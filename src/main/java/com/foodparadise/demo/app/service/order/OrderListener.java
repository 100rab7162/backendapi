package com.foodparadise.demo.app.service.order;

import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;

import java.util.List;

public interface OrderListener {
    public void placeOrder(Order order, List<OrderItemMapping> orderItemMappings);
}
