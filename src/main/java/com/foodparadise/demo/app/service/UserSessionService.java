package com.foodparadise.demo.app.service;

import com.foodparadise.demo.app.model.UserCredential;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.config.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class UserSessionService {

    private static final Logger logger = LoggerFactory.getLogger(UserSessionService.class);

    @Autowired
    UserSession userSession;

    @Autowired
    JWTTokenUtil jwtTokenUtil;

    private Map<String, UserCredential> userCredentialMap;

    @PostConstruct
    public void init(){
        this.userCredentialMap = userSession.getUserSessionMap();
    }



    public UserCredential getUserFromSession(String username){
        UserCredential userCredential = null;

        if(null != userCredentialMap.get(username)){
            userCredential = userCredentialMap.get(username);
        }

        return userCredential;
    }
    public void addUserToSession(UserCredential userCredential){
        userCredentialMap.put(userCredential.getUsername(),userCredential);
        logger.info("Adding user :: " + userCredential.getUsername() + " to session map.");
    }

    public void removeUserFromSession(String username){
        boolean flag = false;

        if(null != userCredentialMap.get(username)){
            userCredentialMap.remove(username);
            flag = true;
        }
        if(flag){
            logger.info("User :: " + username + " is removed from session map.");
        }else{
            logger.info("User :: " + username + " is not found in the session");
        }
    }

}
