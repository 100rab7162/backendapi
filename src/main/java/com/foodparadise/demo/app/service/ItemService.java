package com.foodparadise.demo.app.service;

import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.repository.ItemRepository;
import com.foodparadise.demo.app.service.order.AddOrderToDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    private static final Logger logger = LoggerFactory.getLogger(AddOrderToDatabase.class);

    @Autowired
    ItemRepository itemRepository;

    public List<Item> getItems(){

        return itemRepository.findAll();
    }
}
