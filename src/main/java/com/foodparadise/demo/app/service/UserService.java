package com.foodparadise.demo.app.service;

import com.foodparadise.demo.app.dto.*;
import com.foodparadise.demo.app.model.*;
import com.foodparadise.demo.app.recommender.RecommendByCalorieIntake;
import com.foodparadise.demo.app.recommender.RecommendByCategory;
import com.foodparadise.demo.app.repository.UserCredentialRepository;
import com.foodparadise.demo.app.repository.UserDetailRepository;
import com.foodparadise.demo.app.repository.UserHealthRepository;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.app.util.CommonUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserDetailRepository userDetailRepository;

    @Autowired
    UserHealthRepository userHealthRepository;
    @Autowired
    UserCredentialRepository userCredentialRepository;
    @Autowired
    CommonUtility commonUtil;
    @Autowired
    JWTTokenUtil jwtTokenUtil;
    @Autowired
    UserSessionService userSessionService;
    @Autowired
    RecommendByCalorieIntake recommendByCalorieIntake;
    @Autowired
    RecommendByCategory recommendByCategory;

    public void registerUser(RegistrationDto registrationDto){

        if (checkUserNameExists(registrationDto.getEmail())) {
            logger.info(registrationDto.getEmail() + "already exists!!!");
        } else {
            Timestamp dob = commonUtil.convertStringToTime(registrationDto.getDob());

            UserCredential userCredential = new UserCredential(registrationDto.getEmail(),registrationDto.getPassword(),registrationDto.getType());
            UserDetail userDetail = new UserDetail(registrationDto.getFirstName(),registrationDto.getLastName(),registrationDto.getAddress(),registrationDto.getIpAddress(),dob,registrationDto.getEmail(),registrationDto.getMobile(),userCredential);
            try{
                userDetailRepository.saveAndFlush(userDetail);
            }catch (Exception e){
                logger.error("User Details cannot be saved" + e.getStackTrace());
            }
        }
    }


    public UserHealthResponseDto getUserRecommendation(UserHealthDto userHealthDto){
        BMRCalculator bmrCalculator = new BMRCalculator(userHealthDto.getGender(),userHealthDto.getWeight(),userHealthDto.getHeight(),userHealthDto.getAge(),userHealthDto.getLifeStyle());
        double bmr = bmrCalculator.basalMetabolicRate(userHealthDto.getGender());
        double calorieIntake  = bmrCalculator.calculateCalorieIntake(bmr);
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(userHealthDto.getLifeStyle());
        saveUserHealth(userHealthDto);
        UserHealthResponseDto userHealthResponseDto = new UserHealthResponseDto();
        userHealthResponseDto.setCalorieIntake(calorieIntake);
        userHealthResponseDto.setItems(items);
        return userHealthResponseDto;


    }



    public void saveUserHealth(UserHealthDto userHealthDto){

        UserHealth userHealth = new UserHealth(userHealthDto.getGender(),userHealthDto.getWeight(),userHealthDto.getHeight(),userHealthDto.getAge(),userHealthDto.getLifeStyle());
        if(null == userHealthDto.getUsername() || userHealthDto.getUsername().equals("")){
            userHealthRepository.save(userHealth);
        }else{
            String username = jwtTokenUtil.getUsernameFromToken(userHealthDto.getUsername());
            UserDetail userDetail = userDetailRepository.getUserDetailByEmail(username);
            userDetail.setUserHealth(userHealth);
            userDetailRepository.save(userDetail);
        }
    }

    public List<Item> getRecommendationByCategory(String username,String sortBy){

        List<Item> items = recommendByCategory.recommendItems(username,sortBy);
        items = items.subList(0,3);

        return items;



    }

    public boolean checkUserNameExists(String username){

        if (null != userCredentialRepository.findByUsername(username)) {
            return true;
        } else {
            return false;
        }
    }


    public UserResponseDto authenticateUser(UserLoginRequestDto userLoginRequestDto){
        UserCredential userCredential = null;
        UserResponseDto userResponseDto = null;
        String key = "CIPHERKEY";

        try{

            userCredential = userCredentialRepository.findByUsernameAndPassword(userLoginRequestDto.getUsername(),userLoginRequestDto.getPassword());
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
//            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes("UTF-8"),"AES"));
//            String incomingPassword = new String(cipher.doFinal(Base64.getDecoder().decode(userLoginRequestDto.getPassword())));
//            String databasePassword =  new String(cipher.doFinal(Base64.getDecoder().decode(userCredential.getPassword())));

//            if(incomingPassword.equals(databasePassword)){
//
//            };


        }catch (Exception e){
            logger.error("Error fetching from database :: " + e.getMessage());
        }
        if(null == userCredential){
            userResponseDto = new UserResponseDto(null,null,false);
        }else{
            userResponseDto = new UserResponseDto(jwtTokenUtil.generateToken(userCredential),userCredential.getUsername(),true);
            userSessionService.addUserToSession(userCredential);

        }

        return userResponseDto;
    }

    public void logoutUser(String username){
        userSessionService.removeUserFromSession(username);
    }








}
