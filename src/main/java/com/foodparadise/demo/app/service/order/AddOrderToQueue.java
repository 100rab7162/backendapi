package com.foodparadise.demo.app.service.order;

import com.foodparadise.demo.app.model.Chef;
import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.repository.ChefRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class AddOrderToQueue implements OrderListener {

    private static final Logger logger = LoggerFactory.getLogger(AddOrderToDatabase.class);

    private Queue<Order> orderQueue;
    private volatile Map<Chef,Queue<Order>> chefMap;

    @Autowired
    ChefRepository chefRepository;

    @Value("${chef.queue.size}")
    private Integer chefQueueSize;

    @PostConstruct
    public void init(){
        this.orderQueue = new ArrayDeque<>();
        this.chefMap = new ConcurrentHashMap<>();
        initializeChef();
    }

    @Override
    public void placeOrder(Order order, List<OrderItemMapping> orderItemMappings) {
        orderQueue.offer(order);

        assignOrderToChefQueue(order);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        chefProcessor();
    }

    public void initializeChef(){
        List<Chef> chefs = chefRepository.findAll();

        for (Chef chef : chefs) {
            chefMap.put(chef,new ArrayDeque<>(chefQueueSize));
        }
    }


    public synchronized void  assignOrderToChefQueue(Order order){
        Integer queueSize = null;
        Queue<Order> queue = null;
        Integer min = 100000;
        Chef chefKey = null;
        Set<Map.Entry<Chef,Queue<Order>>> chefSet = chefMap.entrySet();
        for(Map.Entry<Chef,Queue<Order>> entry : chefSet){
            queue = entry.getValue();
            queueSize = queue.size();
            if( queueSize == chefQueueSize){
                continue;
            }
            else if(queueSize < min){
                min = queueSize;
                chefKey = entry.getKey();
            }
        }

        if(null != chefKey){
            Queue queue1 = chefMap.get(chefKey);
            queue1.offer(order);
        }
    }

    public synchronized void chefProcessor(){
        Set<Map.Entry<Chef,Queue<Order>>> chefSet;
        Queue<Order> chefOrderQueue;
        if(null != chefMap){
            chefSet = chefMap.entrySet();
            for(Map.Entry<Chef,Queue<Order>> entry:chefSet){
                chefOrderQueue = entry.getValue();
                if (!chefOrderQueue.isEmpty()) {
                    chefOrderQueue.poll();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    continue;
                }
            }
        }
    }
}
