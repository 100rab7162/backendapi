package com.foodparadise.demo.app.service.order;

import com.foodparadise.demo.app.model.Order;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.repository.OrderItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class AddOrderToDatabase implements OrderListener{

    private static final Logger logger = LoggerFactory.getLogger(AddOrderToDatabase.class);

    @Autowired
    OrderItemRepository orderItemRepository;
    @Override
    public void placeOrder(Order order, List<OrderItemMapping> orderItemMappings) {

        try {
            orderItemRepository.batchInsert(orderItemMappings);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
