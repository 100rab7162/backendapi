package com.foodparadise.demo.app.service;

public class BMRCalculator {

    private String Male;
    private double weight;
    private double height;
    private int age;
    private String lifestyle;

    public BMRCalculator(String male, double weight, double height, int age, String lifestyle) {
        Male = male;
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.lifestyle = lifestyle;
    }

    public double basalMetabolicRate(String male_female){
        if(male_female.equals("male")){
            return 10*weight + 6.25 * height - 5* age + 5;
        }else{
            return 10 * weight + 6.25*height - 5*age - 161;
        }
    }
    public double calculateCalorieIntake(double bmr){
        double newBmr;

        switch (lifestyle){
            case "sedentary": newBmr =  bmr * 1.2;
                break;
            case "slightly_active_lifestyle": newBmr =  bmr * 1.375;
                break;
            case "moderately_active_lifestyle": newBmr =  bmr * 1.55;
                break;
            case "active_lifestyle" : newBmr =  bmr * 1.725;
                break;
            case "very_active_lifestyle":newBmr = bmr * 1.9;
            break;
            default:newBmr = 0.0;
        }
        return newBmr;
    }

}
