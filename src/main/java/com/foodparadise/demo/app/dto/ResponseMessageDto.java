package com.foodparadise.demo.app.dto;

import java.io.Serializable;

public class ResponseMessageDto implements Serializable {
    private String message;

    public ResponseMessageDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
