package com.foodparadise.demo.app.dto;

public class UpdateOrderDto {
    private Integer newRating;
    private Integer orderId;
    private Integer itemId;

    public UpdateOrderDto(){

    }

    public UpdateOrderDto(Integer newRating, Integer orderId, Integer itemId) {
        this.newRating = newRating;
        this.orderId = orderId;
        this.itemId = itemId;
    }

    public Integer getNewRating() {
        return newRating;
    }

    public void setNewRating(Integer newRating) {
        this.newRating = newRating;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
}
