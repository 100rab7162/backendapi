package com.foodparadise.demo.app.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class UserLoginRequestDto implements Serializable {
    @NotBlank(message = "username cannot be empty")
    private String username;
    @NotBlank(message = "password cannot be empty")
    private String password;
//    @NotBlank(message = "message cannot be blank")
    private String ipAddress;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
