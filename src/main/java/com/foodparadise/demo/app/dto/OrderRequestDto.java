package com.foodparadise.demo.app.dto;

import java.io.Serializable;

public class OrderRequestDto implements Serializable {
    private Integer itemId;
    private Integer count;


    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
