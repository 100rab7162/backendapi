package com.foodparadise.demo.app.dto;

import java.io.Serializable;

public class UserResponseDto implements Serializable {
    private String token;
    private String username;
    private Boolean status;

    public UserResponseDto(String token, String username, Boolean status) {
        this.token = token;
        this.username = username;
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getStatus() {
        return status;
    }
}
