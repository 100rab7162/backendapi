package com.foodparadise.demo.app.dto;

import com.foodparadise.demo.app.dto.enumeration.UserType;
import com.foodparadise.demo.app.validator.DOBConstraint;
import com.foodparadise.demo.app.validator.MobileNumberConstraint;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class RegistrationDto implements Serializable {
    @NotBlank(message = "First name cannot be null")
    private String firstName;
    private String lastName;
    @NotBlank(message = "Address cannot be null")
    private String address;

    @NotNull
    private UserType type;




    private String ipAddress;

    @NotBlank(message = "Date of Birth cannot be null")
    @DOBConstraint
    private String dob;
    @Email(message = "This is not a valid email")
    private String email;

    @MobileNumberConstraint
    private String mobile;
    @NotBlank
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
