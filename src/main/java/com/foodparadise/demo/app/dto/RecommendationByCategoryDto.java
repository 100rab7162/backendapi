package com.foodparadise.demo.app.dto;

import java.io.Serializable;

public class RecommendationByCategoryDto implements Serializable {
    private String token;
    private String sortby;

    public RecommendationByCategoryDto(){

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSortby() {
        return sortby;
    }

    public void setSortby(String sortby) {
        this.sortby = sortby;
    }
}
