package com.foodparadise.demo.app.dto;

import com.foodparadise.demo.app.model.FoodCalorieItem;

import java.io.Serializable;
import java.util.List;

public class UserHealthResponseDto implements Serializable {
    private double calorieIntake;
    private List<FoodCalorieItem> items;

    public UserHealthResponseDto(){

    }

    public UserHealthResponseDto(double calorieIntake, List<FoodCalorieItem> items) {
        this.calorieIntake = calorieIntake;
        this.items = items;
    }

    public double getCalorieIntake() {
        return calorieIntake;
    }

    public void setCalorieIntake(double calorieIntake) {
        this.calorieIntake = calorieIntake;
    }

    public List<FoodCalorieItem> getItems() {
        return items;
    }

    public void setItems(List<FoodCalorieItem> items) {
        this.items = items;
    }
}
