package com.foodparadise.demo.app.dto;

import java.io.Serializable;

public class OrderResponseDto implements Serializable {

    private Integer orderId;
    private Boolean status;

    public OrderResponseDto(Integer orderId, Boolean status) {
        this.orderId = orderId;
        this.status = status;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
