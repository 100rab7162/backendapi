package com.foodparadise.demo.app.interceptor;

import com.foodparadise.demo.app.model.UserCredential;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.app.service.UserSessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class TokenAuthentication implements HandlerInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthentication.class);

    @Autowired
    private JWTTokenUtil jwtTokenUtil;
    @Autowired
    private UserSessionService userSessionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //TODO
        boolean flag = false;
        String jToken = request.getHeader("auth_token");
        if (null != jToken && !StringUtils.isEmpty(jToken)) {
            String username = jwtTokenUtil.getUsernameFromToken(jToken);
            //TODO - when handling security
            UserCredential userCredential = userSessionService.getUserFromSession(username);
            //TODO - change here if user to be validated from local cache
            return jwtTokenUtil.validateToken(jToken);
//            flag = (null != userCredential) ? false : jwtTokenUtil.validateToken(jToken);
        }else{
            return false;
        }
//        logger.info("authentication request reaching in TokenAuthentication");
//        return flag;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }
}
