package com.foodparadise.demo.app.recommender;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.repository.ItemRepository;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.CityBlockSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.impl.model.jdbc.MySQLJDBCDataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;

import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class RecommenderSystem {
    @Autowired
    DataSource dataSource;

    @Autowired
    ItemRepository itemRepository;

    public List<Item> recommendItems() {
        List<Item> items = new ArrayList<>();
        try {

            DataModel model = new MySQLJDBCDataModel(dataSource,"order_item","order_id","item_id","rating","");
            CityBlockSimilarity Food_similarity = new CityBlockSimilarity(model);
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1,Food_similarity, model);
            UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, Food_similarity);

            // The First argument is the userID and the Second parameter is 'HOW MANY'
            List<RecommendedItem> recommendations = recommender.recommend(5, 2);
            for (RecommendedItem recommendation : recommendations) {
                int itemId = (int) recommendation.getItemID();
                Item item = itemRepository.findItemById(itemId);
                items.add(item);
            }
        } catch (TasteException e) {
            System.out.println("Exception occured !" +e.getMessage());
        }
        return items;
    }
}



