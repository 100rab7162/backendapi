package com.foodparadise.demo.app.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class CommonUtility {

    @Value("${dob.string.pattern}")
    private String dobStringPattern;

//    public Timestamp convertStringToTime(String dateString){
//        StringPatternToTime stringPattern = new StringPatternToTime();
//        return stringPattern.convertPatternToTime(dateString,dobStringPattern);
//    }



    public Timestamp convertStringToTime(String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dobStringPattern);
        LocalDate localDate = LocalDate.parse(dateString,formatter);
        Timestamp dobTimestamp = Timestamp.valueOf(localDate.atStartOfDay());
        return dobTimestamp;
    }
}
