package com.foodparadise.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class
})
@EnableJpaRepositories(basePackages = "com.foodparadise.demo.app.repository")
public class FoodparadiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodparadiseApplication.class, args);
    }

}
