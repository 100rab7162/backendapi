package com.foodparadise.demo;

import com.foodparadise.demo.app.controller.UserController;
import com.foodparadise.demo.app.dto.OrderRequestDto;
import com.foodparadise.demo.app.dto.RegistrationDto;
import com.foodparadise.demo.app.dto.UserLoginRequestDto;
import com.foodparadise.demo.app.dto.UserResponseDto;
import com.foodparadise.demo.app.dto.enumeration.UserType;
import com.foodparadise.demo.app.model.FoodCalorieItem;
import com.foodparadise.demo.app.model.Item;
import com.foodparadise.demo.app.model.OrderItemMapping;
import com.foodparadise.demo.app.model.UserCredential;
import com.foodparadise.demo.app.recommender.RecommendByCalorieIntake;
import com.foodparadise.demo.app.recommender.RecommendByCategory;
import com.foodparadise.demo.app.security.JWTTokenUtil;
import com.foodparadise.demo.app.service.*;
import com.foodparadise.demo.app.util.CommonUtility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest

class FoodparadiseApplicationTests {
    @Autowired
    ItemService itemService;

    @Autowired
    UserService userService;

    @Autowired
    UserSessionService userSessionService;

    @Autowired
    OrderService orderService;

    @Autowired
    JWTTokenUtil jwtTokenUtil;

    @Autowired
    CommonUtility commonUtility;

    @Autowired
    RecommendByCategory recommendByCategory;

    @Autowired
    RecommendByCalorieIntake recommendByCalorieIntake;


    @Test
    public void testRegisterUser(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisters(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setLastName("testLastName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUserdetails(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUsers(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUservalue(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUservalues(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUserentry(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setLastName("testLastName");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        registrationDto.setPassword("pass");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    @Test
    public void testRegisterUserentries(){
        RegistrationDto registrationDto =  new RegistrationDto();
        registrationDto.setFirstName("testName");
        registrationDto.setDob("1995-09-13");
        registrationDto.setAddress("afjdaskjf");
        registrationDto.setType(UserType.USER);
        registrationDto.setEmail("a@gmail.com");
        userService.registerUser(registrationDto);
        assertTrue(true);
    }
    

    @Test
    public void testUserLogin(){
        UserLoginRequestDto userLoginRequestDto = new UserLoginRequestDto();
        userLoginRequestDto.setUsername("saurabh7517@gmail.com");
        userLoginRequestDto.setPassword("pass");
        UserResponseDto userResponseDto = userService.authenticateUser(userLoginRequestDto);
        assertTrue(userLoginRequestDto.getUsername().equals(userResponseDto.getUsername()));
    }

    @Test
    public void testPlaceOrder(){
        OrderRequestDto orderRequestDto1 = new OrderRequestDto();
        orderRequestDto1.setItemId(1);
        orderRequestDto1.setCount(2);
        OrderRequestDto orderRequestDto2 = new OrderRequestDto();
        orderRequestDto2.setItemId(2);
        orderRequestDto2.setCount(3);
        OrderRequestDto orderRequestDto3 = new OrderRequestDto();
        orderRequestDto3.setItemId(4);
        orderRequestDto3.setCount(1);
        List<OrderRequestDto> orderRequestDtos = new ArrayList<>();
        orderRequestDtos.add(orderRequestDto1);
        orderRequestDtos.add(orderRequestDto2);
        orderRequestDtos.add(orderRequestDto3);
        int x = orderService.registerOrder(orderRequestDtos,"saurabh7517@gmail.com");

        assertTrue(x>0);
    }

    @Test
    public void testGetProducts(){
        List<Item> items = itemService.getItems();
        assertTrue(items.size()>1);
    }

    @Test
    public void testGetOrders(){
        List<OrderItemMapping> items = orderService.getOrders("saurabh7517@gmail.com");
        assertTrue(items.size()>1);
    }

    @Test
    public void testJWTTokenLibrary(){
        UserCredential userCredential = new UserCredential();
        userCredential.setUsername("test@gmail.com");
        String encryptedValue = jwtTokenUtil.generateToken(userCredential);
        assertTrue(jwtTokenUtil.validateToken(encryptedValue,userCredential));
    }

    @Test
    public void timeConverterLibrary(){
        String time = "1990-09-09";
        Timestamp timestamp = commonUtility.convertStringToTime(time);
        assertTrue(true);

    }

    @Test
    public void testBMRCalculatorLibrary(){
        BMRCalculator bmrCalculator = new BMRCalculator("male",70,172,29,"sedentary");
        double bmr = bmrCalculator.basalMetabolicRate("male");
        double calories = bmrCalculator.calculateCalorieIntake(bmr);
        assertTrue(calories > 0.0);
    }

    @Test
    public void testRecommendationByGluten(){
        String username = "test@gmail.com";
        List<Item> items = recommendByCategory.recommendItems(username,"gluten");
        assertTrue(items.size()>0);
    }

    @Test
    public void testRecommendationByVegan(){
        String username = "test@gmail.com";
        List<Item> items = recommendByCategory.recommendItems(username,"veg");
        assertTrue(items.size()>0);
    }

    @Test
    public void testRecommendationBySpicy(){
        String username = "test@gmail.com";
        List<Item> items = recommendByCategory.recommendItems(username,"spicy");
        assertTrue(items.size()>0);
    }

    @Test
    public void testRecommendCalorieIntakeBySedentaryLifeStyle(){
        String lifeStyle = "sedentary";
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(lifeStyle);
        FoodCalorieItem foodCalorieItem = items.get(1);
        assertTrue(foodCalorieItem.getCarbohydrates_100g()>20 && foodCalorieItem.getCarbohydrates_100g()<50);
    }

    @Test
    public void testRecommendCalorieIntakeBySlightlyActiveLifeStyle(){
        String lifeStyle = "slightly_active_lifestyle";
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(lifeStyle);
        FoodCalorieItem foodCalorieItem = items.size()>0 ? items.get(0) : null;
        assertTrue(foodCalorieItem.getProteins_100g()>30 && foodCalorieItem.getProteins_100g()<50);
    }

    @Test
    public void testRecommendCalorieIntakeByModeratelyActiveLifeStyle(){
        String lifeStyle = "moderately_active_lifestyle";
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(lifeStyle);
        FoodCalorieItem foodCalorieItem = items.size()>0 ? items.get(0) : null;
        assertTrue(foodCalorieItem.getProteins_100g()>30 && foodCalorieItem.getProteins_100g()<50);
    }

    @Test
    public void testRecommendCalorieIntakeByActiveLifeStyles(){
        String lifeStyle = "active_lifestyle";
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(lifeStyle);
        FoodCalorieItem foodCalorieItem = items.size()>0 ? items.get(0) : null;
        assertTrue(foodCalorieItem.getProteins_100g()>60);
    }

    @Test
    public void testRecommendCalorieIntakeByVeryActiveLifeStyle(){
        String lifeStyle = "very_active_lifestyle";
        List<FoodCalorieItem> items = recommendByCalorieIntake.recommendItems(lifeStyle);
        FoodCalorieItem foodCalorieItem = items.size()>0 ? items.get(0) : null;
        assertTrue(foodCalorieItem.getProteins_100g()>60);
    }










}
